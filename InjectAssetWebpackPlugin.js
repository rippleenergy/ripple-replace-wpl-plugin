
const fs = require('fs');
const path = require('path');

class InjectAssetWebpackPlugin {

  constructor(context, templateDir, files) {
    this.templateDir = templateDir;
    this.files = files;
    this.context = context;
  }

  apply(compiler) {
    compiler.hooks.emit.tap('Inject Assets Plugin', (
      compilation
    ) => {
        //console.log(Object.keys(compilation));
        let m = {};
        compilation.entrypoints.forEach((e) => {
            let l = '';
            e.chunks.forEach( (c) => {
                c.files.forEach( (fileName) => {
                    l += '<script type="text/javascript" src="{% static \'bundles/prod/' + fileName + '\' %}"></script>\n';
                });
            });
            m[e.name] = l
        });

        this.files.forEach(
            (f) => {
                let fin = path.resolve(this.context, this.templateDir, f+'_wpl.html');
                let fout = path.resolve(this.context, this.templateDir, f+'.html');
                let content = fs.readFileSync(fin, 'utf8');
                // replace
                content = content.replace(/\{\% load render_bundle from webpack_loader \%\}\s*[\r\n]{1,2}/mg, '');

                Object.keys(m).forEach(
                    function(a) {
                        let r = RegExp('\\{\\%\\s*render_bundle\\s*\''+ a +'\'\\s*\'js\'\\s*\\%\\}', 'g');
                        content = content.replace(r, m[a]);
                    }
                );
                fs.writeFileSync(fout, content);
            }
        );
    });
  }

}

module.exports =  InjectAssetWebpackPlugin;